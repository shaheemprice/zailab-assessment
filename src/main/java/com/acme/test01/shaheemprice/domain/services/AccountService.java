package com.acme.test01.shaheemprice.domain.services;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

public interface AccountService {
	public void openSavingsAccount(Long accountId, int amountToDeposit);

	public void openCurrentAccount(Long accountId);

	public void withdraw(Long accountId, int amountToWithdraw)
			throws AccountNotFoundException, WithdrawalAmountTooLargeException;

	public void deposit(Long accountId, int amountToDeposit) throws AccountNotFoundException;
}
