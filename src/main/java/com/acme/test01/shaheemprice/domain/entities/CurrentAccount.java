package com.acme.test01.shaheemprice.domain.entities;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

public class CurrentAccount extends BankAccount {

	private int overdraft;

	public CurrentAccount(Long accountId, String customerNumber, int balance, int overdraft) {
		super(accountId, customerNumber, balance);

		this.overdraft = overdraft;

	}

	@Override
	public void deposit(int amountToDeposit) {
		this.balance += amountToDeposit;

	}

	@Override
	public void withdraw(int amountToWithdraw) throws WithdrawalAmountTooLargeException {
		int overdraftBalance = overdraft + balance;

		if (amountToWithdraw > overdraftBalance) {
			throw new WithdrawalAmountTooLargeException(
					String.format(WithdrawalAmountTooLargeException.OVERDRAWN, amountToWithdraw, overdraftBalance));
		}

		balance -= amountToWithdraw;

	}

	public int getOverdraft() {
		return this.overdraft;
	}

}
