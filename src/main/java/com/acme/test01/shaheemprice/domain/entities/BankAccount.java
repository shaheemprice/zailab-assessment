package com.acme.test01.shaheemprice.domain.entities;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

public abstract class BankAccount {
	protected Long accountId;
	protected int balance;
	protected String customerNumber;
	
	
	protected BankAccount(Long accountId, String customerNumber, int balance) {
		
		this.balance = balance;
		this.customerNumber = customerNumber;
		
		this.accountId = accountId;
	}
	
	/**
	 * 
	 * @param amountToDeposit
	 */
	public abstract void deposit(int amountToDeposit);
	
	/**
	 * 
	 * @param amountToWithdraw
	 */
	public abstract void withdraw(int amountToWithdraw) throws WithdrawalAmountTooLargeException;

	public Long getAccountId() {
		return accountId;
	}

	public int getBalance() {
		return balance;
	}

	public String getCustomerNumber() {
		return customerNumber;
	}
}
