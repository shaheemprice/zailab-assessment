package com.acme.test01.shaheemprice.domain.exceptions;

public class BusinessExceptions {
	
	public static class WithdrawalAmountTooLargeException extends RuntimeException {

		private static final long serialVersionUID = 1L;
		
		public static final String BELOW_LIMIT = "The minimum balance for a Savings Account is %d";
		public static final String OVERDRAWN = "The requested amount: %d exceeds the available amount: %d";

		public WithdrawalAmountTooLargeException(String message) {
			super(message);
		}
	}
	
	public static class AccountNotFoundException extends Exception {

		private static final long serialVersionUID = 1L;
		
		public static final String NOT_FOUND = "No account found for id: %d";

		public AccountNotFoundException(String message) {
			super(message);
		}
		
	}
}
