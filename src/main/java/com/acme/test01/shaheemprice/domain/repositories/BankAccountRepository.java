package com.acme.test01.shaheemprice.domain.repositories;

import java.util.List;

import com.acme.test01.shaheemprice.domain.entities.BankAccount;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;

public interface BankAccountRepository {
	void openAccount(BankAccount bankAccount);

	BankAccount retrieveAccount(Long accountId) throws AccountNotFoundException;

	List<BankAccount> listAccounts();
}
