package com.acme.test01.shaheemprice.domain.entities;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

public class SavingsAccount extends BankAccount {
	private final static int DEPOSIT_LIMIT = 1000;

	public SavingsAccount(Long accountId, String customerNumber, int balance) {
		super(accountId, customerNumber, balance);
	}

	@Override
	public void deposit(int amountToDeposit) {
		balance += amountToDeposit;
	}

	@Override
	public void withdraw(int amountToWithdraw) throws WithdrawalAmountTooLargeException {
		if (this.balance - amountToWithdraw > DEPOSIT_LIMIT) {
			this.balance -= amountToWithdraw;
		} else {
			throw new WithdrawalAmountTooLargeException(String.format(WithdrawalAmountTooLargeException.BELOW_LIMIT, DEPOSIT_LIMIT));
		}
	}

}
