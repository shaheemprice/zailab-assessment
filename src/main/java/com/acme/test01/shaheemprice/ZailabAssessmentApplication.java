package com.acme.test01.shaheemprice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZailabAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZailabAssessmentApplication.class, args);
	}
}
