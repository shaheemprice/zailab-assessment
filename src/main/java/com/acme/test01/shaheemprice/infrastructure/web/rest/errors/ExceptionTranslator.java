package com.acme.test01.shaheemprice.infrastructure.web.rest.errors;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

/**
 * Controller advice to translate the server side exceptions to client-friendly json structures.
 */
@ControllerAdvice
public class ExceptionTranslator {
	Logger log = LoggerFactory.getLogger(ExceptionTranslator.class);
	
	@ExceptionHandler(WithdrawalAmountTooLargeException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestErrorMessage processWithdrawalAmountTooLargeException(WithdrawalAmountTooLargeException ex) {
		return new RestErrorMessage(ex.getMessage());
	}
	
	@ExceptionHandler(AccountNotFoundException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestErrorMessage processAccountNotFoundException(AccountNotFoundException ex) {
		return new RestErrorMessage(ex.getMessage());
	}
    
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestErrorMessage processNumberFormatException(NumberFormatException ex) {
    	return new RestErrorMessage(ErrorConstants.ERR_VALIDATION, ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public RestErrorMessage processValidationError(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();

        return processFieldErrors(fieldErrors);
    }

    @ExceptionHandler(CustomParameterizedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ParameterizedErrorDTO processParameterizedValidationError(CustomParameterizedException ex) {
        return ex.getErrorDTO();
    }

    private RestErrorMessage processFieldErrors(List<FieldError> fieldErrors) {
        RestErrorMessage dto = new RestErrorMessage(ErrorConstants.ERR_VALIDATION);

        for (FieldError fieldError : fieldErrors) {
            dto.add(fieldError.getObjectName(), fieldError.getField(), fieldError.getCode());
        }

        return dto;
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    public RestErrorMessage processMethodNotSupportedException(HttpRequestMethodNotSupportedException exception) {
        return new RestErrorMessage(ErrorConstants.ERR_METHOD_NOT_SUPPORTED, exception.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RestErrorMessage> processRuntimeException(Exception ex) throws Exception {
        BodyBuilder builder;
        RestErrorMessage errorDTO;
        ResponseStatus responseStatus = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class);
        if (responseStatus != null) {
            builder = ResponseEntity.status(responseStatus.value());
            errorDTO = new RestErrorMessage("error." + responseStatus.value().value(), responseStatus.reason());
        } else {
        	log.error(ex.getMessage());
            builder = ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR);
            errorDTO = new RestErrorMessage(ErrorConstants.ERR_INTERNAL_SERVER_ERROR, "Internal server error");
        }
        return builder.body(errorDTO);
    }
}
