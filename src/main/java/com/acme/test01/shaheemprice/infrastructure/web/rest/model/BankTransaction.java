package com.acme.test01.shaheemprice.infrastructure.web.rest.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({ @JsonSubTypes.Type(value = WithdrawalTransaction.class, name = "Withdrawal"),
		@JsonSubTypes.Type(value = DepositTransaction.class, name = "Deposit") })
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public abstract class BankTransaction implements Serializable {
	private static final long serialVersionUID = 1L;

	@NotNull
	private int amount;

	public BankTransaction() {
	}

	public BankTransaction(int amount) {
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}
}
