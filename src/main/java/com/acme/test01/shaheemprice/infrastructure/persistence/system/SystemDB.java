package com.acme.test01.shaheemprice.infrastructure.persistence.system;

import java.util.HashMap;
import java.util.Map;

import com.acme.test01.shaheemprice.domain.entities.BankAccount;
import com.acme.test01.shaheemprice.domain.entities.CurrentAccount;
import com.acme.test01.shaheemprice.domain.entities.SavingsAccount;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;

public class SystemDB {
	private Map<Long, BankAccount> accounts = new HashMap<Long, BankAccount>();

	public final static SystemDB instance = new SystemDB();

	private SystemDB() {
		createBankAccount(new SavingsAccount(new Long(1), "1", 2000));

		createBankAccount(new SavingsAccount(new Long(2), "2", 5000));

		createBankAccount(new CurrentAccount(new Long(3),"3", 1000, 10000));

		createBankAccount(new CurrentAccount(new Long(4), "4", -5000, 20000));
	}

	public BankAccount retrieveBankAccount(Long id) throws AccountNotFoundException {
		BankAccount result = accounts.get(id);

		if (result == null)
			throw new AccountNotFoundException(String.format(AccountNotFoundException.NOT_FOUND, id));

		return result;
	}

	public Map<Long, BankAccount> retrieveAllBankAccounts() {

		return accounts;
	}

	public void createBankAccount(BankAccount bankAccount) {
		accounts.put(bankAccount.getAccountId(), bankAccount);
	}
}
