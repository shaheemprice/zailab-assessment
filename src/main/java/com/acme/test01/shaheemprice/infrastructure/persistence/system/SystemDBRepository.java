package com.acme.test01.shaheemprice.infrastructure.persistence.system;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.acme.test01.shaheemprice.domain.entities.BankAccount;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;
import com.acme.test01.shaheemprice.domain.repositories.BankAccountRepository;

@Component
public class SystemDBRepository implements BankAccountRepository {

	@Override
	public void openAccount(BankAccount bankAccount) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	public BankAccount retrieveAccount(Long accountId) throws AccountNotFoundException {
		return SystemDB.instance.retrieveBankAccount(accountId);
	}

	@Override
	public List<BankAccount> listAccounts() {
		Map<Long, BankAccount> bankAccounts = SystemDB.instance.retrieveAllBankAccounts();

		List<BankAccount> result = new ArrayList<BankAccount>();

		for (Map.Entry<Long, BankAccount> entry : bankAccounts.entrySet()) {
			result.add(entry.getValue());
		}

		return result;
	}

}
