package com.acme.test01.shaheemprice.infrastructure.web.rest;

import java.net.URISyntaxException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;
import com.acme.test01.shaheemprice.domain.services.AccountService;
import com.acme.test01.shaheemprice.infrastructure.web.rest.model.BankTransaction;

@RestController
@RequestMapping("/bank-accounts")
public class BankAccountResource {
	@Autowired
	private AccountService accountService;
	
	@RequestMapping(value="/{id}/transactions", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> transact(@PathVariable Long id, @Valid @RequestBody BankTransaction bankTransaction) 
			throws URISyntaxException, AccountNotFoundException, WithdrawalAmountTooLargeException {
		
		if (bankTransaction.getClass().getSimpleName().equals("DepositTransaction")) {
			accountService.deposit(id, bankTransaction.getAmount());
		} else if (bankTransaction.getClass().getSimpleName().equals("WithdrawalTransaction")) {
			accountService.withdraw(id, bankTransaction.getAmount());
		}
		
		return ResponseEntity
				.accepted()
				.body("[\"status: success\"]");		// something more meaningful can be returned here, such as the account with its balance after transaction.
	}
}
