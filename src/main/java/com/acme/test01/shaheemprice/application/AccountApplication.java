package com.acme.test01.shaheemprice.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.acme.test01.shaheemprice.domain.entities.BankAccount;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.AccountNotFoundException;
import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;
import com.acme.test01.shaheemprice.domain.repositories.BankAccountRepository;
import com.acme.test01.shaheemprice.domain.services.AccountService;

@Service
public class AccountApplication implements AccountService {

	@Autowired
	private BankAccountRepository accountRepository;

	@Override
	public void openSavingsAccount(Long accountId, int amountToDeposit) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	public void openCurrentAccount(Long accountId) {
		throw new UnsupportedOperationException("Not implemented yet");
	}

	@Override
	public void withdraw(Long accountId, int amountToWithdraw)
			throws AccountNotFoundException, WithdrawalAmountTooLargeException {
		// locate account
		BankAccount bankAccount = accountRepository.retrieveAccount(accountId);

		// withdraw
		bankAccount.withdraw(amountToWithdraw);
	}

	@Override
	public void deposit(Long accountId, int amountToDeposit) throws AccountNotFoundException {
		// locate account
		BankAccount bankAccount = accountRepository.retrieveAccount(accountId);

		// deposit
		bankAccount.deposit(amountToDeposit);
	}

}
