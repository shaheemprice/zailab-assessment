package com.acme.test01.shaheemprice.domain.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.acme.test01.shaheemprice.domain.exceptions.BusinessExceptions.WithdrawalAmountTooLargeException;

public class SavingsAccountTest {
	private final static Long ACCOUNT_ID = 1L;
	private final static String CUSTOMER_NUMBER = "2";
	private final static String AMOUNT_TOO_LARGE_NOT_EXPECTED = "WithdrawalAmountTooLargeException not expected";
	
	private Fixture fixture;

	@Rule
	public final ExpectedException expectedException = ExpectedException.none();

	@Before
	public void setup() {
		fixture = new Fixture();
	}

	@Test
	public void afterConstruction() {
		fixture.givenSavingsAccount(ACCOUNT_ID, CUSTOMER_NUMBER, 1000);
		fixture.thenAccountId_is(ACCOUNT_ID);
		fixture.thenBalance_is(1000);
		fixture.thenCustomerNumber_is(CUSTOMER_NUMBER);
	}

	@Test
	public void depositFunds() {
		fixture.givenSavingsAccount(ACCOUNT_ID, CUSTOMER_NUMBER, 1000);
		fixture.givenDeposit_of(2000);
		fixture.whenDeposit_isInvoked();
		fixture.thenBalance_is(3000);
	}

	@Test
	public void withdrawFundsWithinBalance() {
		fixture.givenSavingsAccount(ACCOUNT_ID, CUSTOMER_NUMBER, 2000);
		fixture.givenWithdrawal_of(200);
		try {
			fixture.whenWithdraw_isInvoked();
		} catch (WithdrawalAmountTooLargeException e) {
			fail(AMOUNT_TOO_LARGE_NOT_EXPECTED);
		}
		fixture.thenBalance_is(1800);
	}

	@Test
	public void withdrawInsufficientFunds() throws WithdrawalAmountTooLargeException {
		fixture.givenSavingsAccount(ACCOUNT_ID, CUSTOMER_NUMBER, 1000);
		fixture.givenWithdrawal_of(1000);

		expectedException.expect(WithdrawalAmountTooLargeException.class);
		expectedException
				.expectMessage(String.format(WithdrawalAmountTooLargeException.BELOW_LIMIT, 1000));

		fixture.whenWithdraw_isInvoked();
	}

	private class Fixture {

		private SavingsAccount savingsAccount;
		private int deposit;
		private int withdraw;

		private void givenSavingsAccount(Long accountId, String customerNumber, int balance) {
			savingsAccount = new SavingsAccount(accountId, customerNumber, balance);
		}

		public void whenDeposit_isInvoked() {
			this.savingsAccount.deposit(deposit);
		}

		public void givenDeposit_of(int deposit) {
			this.deposit = deposit;
		}

		public void givenWithdrawal_of(int withdraw) {
			this.withdraw = withdraw;
		}

		public void whenWithdraw_isInvoked() throws WithdrawalAmountTooLargeException {
			this.savingsAccount.withdraw(withdraw);
		}

		private void thenAccountId_is(Long expectedAccountId) {
			assertEquals(expectedAccountId, savingsAccount.getAccountId());
		}

		private void thenBalance_is(int expectedBalance) {
			assertEquals(expectedBalance, savingsAccount.getBalance());
		}

		private void thenCustomerNumber_is(String expectedCustomerNumber) {
			assertEquals(expectedCustomerNumber, savingsAccount.getCustomerNumber());
		}
	}
}
