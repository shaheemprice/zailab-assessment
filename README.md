I have implemented the assessment as a Spring Boot Microservice.

The architecture is based on the architecture I put in place at Metropolitan and makes use of DDD and the Hexagonal (Ports/Adapters) architecture.
The intention of the architecture is to have a pure a POJO business core devoid of any external frameworks or containers. This makes it easier to unit test and to swop frameworks without breaking business code.
All external concerns are relegated to the Infrastructure package, these are the adapters.

The REST interface makes use of JSON "inheritance" to provide one endpoint for transactions which is extendable by defining now classes in the rest model which extend the base transaction.

Things that I would improve on for this exercise is to make the methods of the service interface not be void methods. It makes unit testing difficult. I would also look into using the Strategy pattern for an account id generator.
Different databases use different ways to generate auto-incrementing IDs. Each DB adapter could then provide a strategy specific to that DB.

I would also look at using an interface for business rules and using a rules engine. Something like Easy rules http://www.easyrules.org/ as a start.
